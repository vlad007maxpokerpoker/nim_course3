#ifndef POSITION_H
#define POSITION_H

#include <vector>

class Position {
public:
    Position(int n);
    Position(const Position& other);
    int GetRange() const;
    void MakeMMove();
    void SortX();
    bool IsCorrectTheorem1() const;
    void PrintX() const;
private:
    int n_;
    std::vector<int> x_;
    bool IsRightP1() const;
    bool IsRightP2() const;
    bool IsRightP3() const;
    bool IsRightP4() const;
    bool IsRightP5() const;
};

#endif // POSITION_H
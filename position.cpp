#include "position.h"

#include <algorithm>
#include <cassert>
#include <iostream>
#include <time.h>

namespace {
    constexpr int N_MIN_VALUE = 2;
    constexpr int XI_MIN_VALUE = 1001;
    constexpr int XI_RUNUP = 50;
} // namespace

/**
 * Constructor of Position. In x_ sets random numbers from 1001 to 1050
*/
Position::Position(int n) : n_(n)
{
    srand(time(NULL));
    assert(n_ >= N_MIN_VALUE);
    x_.resize(n_);
    for (int i = 0; i < n_; ++i) {
        x_[i] = XI_MIN_VALUE + rand() % XI_RUNUP;
    }
    std::fill(x_.begin(), x_.end(), 0);
}

/**
 * Copy constructor of Position
*/
Position::Position(const Position& other)
{
    n_ = other.n_;
    assert(n_ >= N_MIN_VALUE);
    x_.resize(n_);
    assert(x_.size() == other.x_.size());
    std::copy(other.x_.begin(), other.x_.end(), x_.begin());
}

/**
 * @return range(x_) = max(x_i) - min(x_i)
*/
int Position::GetRange() const
{
    return *std::max_element(x_.begin(), x_.end()) - *std::min_element(x_.begin(), x_.end());
}

/**
 * Make M-move in the Slow NIM.
 * If there if even number then we keep the smallest even number.
 * Otherwise we keep the biggest number.
*/
void Position::MakeMMove()
{
    // check if there is even number
    bool isThereEven = false;
    for (const int elem: x_) {
        if (elem & 1 == 0) {
            isThereEven = true;
            break;
        } 
    }
    int positionToKeep = -1;
    int valueToKeep = 0;
    if (isThereEven) {
        // if we have at least one even number then we need to keep the smallest even
        valueToKeep = INT_MAX;
        for (int i = 0; i < n_; ++i) {
            if (x_[i] & 1 == 0) {
                if (x_[i] < valueToKeep) {
                    valueToKeep = x_[i];
                    positionToKeep = i;
                }
            }
        }
    } else {
        // otherwise we need to keep the biggest
        valueToKeep = INT_MIN;
        for (int i = 0; i < n_; ++i) {
            if (x_[i] > valueToKeep) {
                valueToKeep = x_[i];
                positionToKeep = i;
            }
        }
    }
    // now we save keeping element and substract 1 from others elements
    for (int i = 0; i < n_; ++i) {
        if (i != positionToKeep) {
            --x_[i];
        }
    }
}

/**
 * n >= 2
 * This property always works, since such a check was performed during the Position constructor.
*/
bool Position::IsRightP1() const
{
    return n_ >= 2;
}

/**
 * range(x) <= 2
*/
bool Position::IsRightP2() const
{
    return GetRange() <= 2;
}

/**
 * x_i >= 2n-2 for all i
*/
bool Position::IsRightP3() const
{
    return *std::min_element(x_.begin(), x_.end()) >= 2 * n_ - 2;
}

/**
 * x_i is even for at least one i
*/
bool Position::IsRightP4() const
{
    for (const int elem: x_) {
        if (elem & 1 == 0) {
            return true;
        }
    }
    return false;
}

/**
 * Let's x = (x_1, x_2, ..., x_n)
 * Then after 2n M-moves we will get x' = (x_1 - 2(n-1), x_2 - 2(n-1), ..., x_n - 2(n-1))
*/
bool Position::IsRightP5() const
{
    Position stone(*this);
    Position traveler(*this);
    for (int i = 0; i < 2 * n_; ++i) {
        traveler.MakeMMove();
    }
    stone.SortX();
    traveler.SortX();
    for (int i = 0; i < n_; ++i) {
        if (traveler.x_[i] + 2 * (n_ - 1) != stone.x_[i]) {
            return false;
        }
    }
    return true;
}

/**
 * Sort elements in x_ in ascending order
*/
void Position::SortX()
{
    std::sort(x_.begin(), x_.end());
}

/**
 * Check that p1 & p2 & p3 & p4 are equivalent that p5
*/
bool Position::IsCorrectTheorem1() const
{
    bool LHS = IsRightP1() && IsRightP2() && IsRightP3() && IsRightP4();
    bool RHS = IsRightP5();
    return !(LHS ^ RHS);
}

/**
 * Print x_
*/
void Position::PrintX() const
{
    for (const int elem: x_) {
        std::cout << elem << " ";
    }
    std::cout << "\n";
}
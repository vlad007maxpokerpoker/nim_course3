#include "position.h"

#include <iostream>

namespace {
    constexpr int NUM_ITERATIONS = 1e7;
} // namespace

int main() {
    for (int n = 2; n <= 10; ++n) {
        for (int iter = 0; iter <= NUM_ITERATIONS; ++iter) {
            Position pos(n);
            if (!pos.IsCorrectTheorem1()) {
                std::cout << "There is position that is not correct with Theorem 1.\n";
                pos.PrintX();
                return 1;
            }
        }
    }
    std::cout << "All positions are correct with Theorem 1!\n";
    return 0;
}